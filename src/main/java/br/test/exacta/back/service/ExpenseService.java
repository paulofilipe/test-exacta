package br.test.exacta.back.service;

import br.test.exacta.back.model.Expense;
import br.test.exacta.back.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ExpenseService extends AbstractService<Expense> {

    @Autowired
    public ExpenseService(ExpenseRepository repository) {
        super(repository);
    }

    public Expense findById(Long id) {
        Optional<Expense> expense = repository.findById(id);

        return expense.orElseThrow(() -> new NoSuchElementException("Expense not found"));
    }
}
