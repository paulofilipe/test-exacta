package br.test.exacta.back.service;

import java.util.List;

public interface IService<T> {
    void insert(T model);
    T findById(Long id);
    List<T> findAll();
    void delete(Long id);
    void update(T model);
}
