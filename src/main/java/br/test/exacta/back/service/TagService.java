package br.test.exacta.back.service;

import br.test.exacta.back.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TagService extends AbstractService<Tag> {

    public TagService(JpaRepository<Tag, Long> repository) {
        super(repository);
    }

    @Override
    public Tag findById(Long id) {
        Optional<Tag> tag = repository.findById(id);

        return tag.orElseThrow(() -> new NoSuchElementException("Tag not found"));
    }
}
