package br.test.exacta.back.repository;

import br.test.exacta.back.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {
}
