package br.test.exacta.back.controler;

import br.test.exacta.back.model.Expense;
import br.test.exacta.back.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/expense")
public class ExpenseController extends BaseController<Expense> {

    @Autowired
    public ExpenseController(IService<Expense> service) {
        super(service);
    }
}
