package br.test.exacta.back.controler;

import br.test.exacta.back.service.IService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class BaseController<T> {

    private IService<T> service;

    public BaseController(IService<T> service) {
        this.service = service;
    }


    @GetMapping(value = "/")
    public ResponseEntity<List<T>> listaAll() {

        try {
            List<T> list = service.findAll();
            if (list == null) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping(value = "/")
    public ResponseEntity<T> create(@RequestBody T model) {
        service.insert(model);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(value = "/")
    public ResponseEntity<T> update(@RequestBody T model) {
        service.update(model);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<T> get(@PathVariable("id") Long id) {
        T T;
        try {
            T = service.findById(id);
            if (T == null) {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(T);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<T> delete(@PathVariable("id") Long id) {
        service.delete(id);
        return ResponseEntity.status(HttpStatus.GONE).build();
    }
}
