package br.test.exacta.back.controler;

import br.test.exacta.back.model.Tag;
import br.test.exacta.back.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tag")
public class TagController extends BaseController<Tag> {

    @Autowired
    public TagController(IService<Tag> service) {
        super(service);
    }
}
