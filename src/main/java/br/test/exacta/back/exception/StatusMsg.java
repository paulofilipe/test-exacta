package br.test.exacta.back.exception;

public enum StatusMsg {
    INFO,
    WARN,
    ERROR,
    FATAL;
}
