package br.test.exacta.back.exception;

import java.time.LocalDateTime;

public class MsgException {

    private final LocalDateTime occurrenceDate;
    private final String message;
    private final String details;
    private final StatusMsg status;

    public MsgException(String mensagem, String detalhes, StatusMsg status) {
        this.occurrenceDate = LocalDateTime.now();
        this.message = mensagem;
        this.status = status;
        this.details = detalhes;
    }

    public LocalDateTime getOccurrenceDate() {
        return occurrenceDate;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public StatusMsg getStatus() {
        return status;
    }

}
