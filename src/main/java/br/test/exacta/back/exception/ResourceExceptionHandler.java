package br.test.exacta.back.exception;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResourceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<MsgException> handleUserNotFoundException(ConstraintViolationException ex, WebRequest request) {
        logger.error("Constraint violation", ex);
        MsgException errorDetails = new MsgException(ex.getMessage(), request.getDescription(false), StatusMsg.ERROR);
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<MsgException> exception(Exception ex, WebRequest request) {
        logger.error("Generic exception", ex);
        MsgException errorDetails = new MsgException(ex.getMessage(), request.getDescription(false), StatusMsg.ERROR);
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
